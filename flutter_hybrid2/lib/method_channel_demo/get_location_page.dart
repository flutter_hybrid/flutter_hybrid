import 'package:flutter/material.dart';

import 'location_plugin.dart';

///MethodChannel：用于传递方法调用（method invocation）（双向有返回值）。
///注意：刚开始获取时报错，请手动开启定位权限
class GetLocationPage extends StatefulWidget {
  const GetLocationPage({Key? key}) : super(key: key);

  @override
  State<GetLocationPage> createState() => _GetLocationPageState();
}

class _GetLocationPageState extends State<GetLocationPage>
    implements LocationCallback {
  String _gpsLocation = '';
  String _registerAddress = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('获取位置信息'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                  onPressed: () {
                    LocationPlugin.setHintInfo('开始获取');
                    LocationPlugin.getLocation(this);
                  },
                  child: Text('点击获取信息')),
              SizedBox(height: 100),
              Text('经纬度值：$_gpsLocation'),
              SizedBox(height: 100),
              Text('地址值：$_registerAddress'),
            ],
          ),
        ));
  }

  @override
  void onGetLocationResult(bool isSuccess, Map resultMap) {
    if (isSuccess) {
      print(
          'locationLatitude:${resultMap['latitude']}  longitude:${resultMap['longitude']} address:${resultMap['address']}');

      _gpsLocation = '${resultMap['longitude']},${resultMap['latitude']}';
      _registerAddress = resultMap['address'];
      setState(() {});
    }
  }
}
