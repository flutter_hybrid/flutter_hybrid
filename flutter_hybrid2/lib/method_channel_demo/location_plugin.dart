import 'package:flutter/services.dart';

//callback interface
abstract class LocationCallback {
  void onGetLocationResult(bool isSuccess, Map resultMap);
}

//获取位置信息插件
class LocationPlugin {
  ///Channel的名字
  static const MethodChannel _channel = const MethodChannel('location_plugin');

  static void getLocation(LocationCallback locationCallback) {
    Future<String> locationCall(MethodCall methodCall) async {
      print('locationMethodCall:${methodCall.method}');
      print('locationMethodCall:${(methodCall.arguments as Map).toString()}');

      switch (methodCall.method) {
        case "onGetLocationSuccess":
          locationCallback.onGetLocationResult(
              true, (methodCall.arguments as Map));
          break;
        case "onGetLocationFail":
          locationCallback.onGetLocationResult(
              false, (methodCall.arguments as Map));
          break;
      }
      return "";
    }

    _channel.setMethodCallHandler(locationCall);
    _channel.invokeMethod("getLocation");
  }

  ///获取前给原生端传个参数
  static void setHintInfo(String info) {
    _channel.invokeMethod("setHintInfo", {'info': info});
  }
}
