import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

///EventChannel: 用于数据流（event streams）的通信（仅支持数据单向传递，无返回值）。
class EventChannelDemoPage extends StatefulWidget {
  const EventChannelDemoPage({Key? key}) : super(key: key);

  @override
  State<EventChannelDemoPage> createState() => _EventChannelDemoPageState();
}

class _EventChannelDemoPageState extends State<EventChannelDemoPage> {
  static const EventChannel _eventChannel =
      const EventChannel('EventChannelDemo');

  String _changeVoiceValue = '';

  void initState() {
    super.initState();
    // 监听开始 原生发来数据
    _eventChannel
        .receiveBroadcastStream()
        .listen(_onEvent, onError: _onError, onDone: () {});
  }

  void _onEvent(event) {
    final Map<dynamic, dynamic> map = event;
    if (map.containsKey("changeVoice")) {
      //声音改变监听值
      setState(() {
        _changeVoiceValue = '${map["changeVoice"]}';
      });
    }
  }

  void _onError(error) {
    print('Flutter - 返回的错误:$error');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('EventChannelDemoPage'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 100),
              Text('声音改变监听值：$_changeVoiceValue'),
            ],
          ),
        ));
  }
}
