import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

///BasicMessageChannel：用于传递字符串和半结构化的信息（双向有返回值）。
class BasicMessageChannelDemoPage extends StatefulWidget {
  const BasicMessageChannelDemoPage({Key? key}) : super(key: key);

  @override
  State<BasicMessageChannelDemoPage> createState() =>
      _BasicMessageChannelDemoPageState();
}

class _BasicMessageChannelDemoPageState
    extends State<BasicMessageChannelDemoPage> {
  static const BasicMessageChannel _basicMessageChannel =
      const BasicMessageChannel(
          'basic_message_channel_demo', StandardMessageCodec());
  String _infoStr = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('BasicMessageChannelDemoPage'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                  onPressed: () async {
                    // 向原生发送 数据 并接收返回数据
                    _infoStr = await _basicMessageChannel.send('flutter端传过来的值');
                    setState(() {});
                  },
                  child: Text('点击给原生传递信息')),
              SizedBox(height: 100),
              Text('原生传过来的值：$_infoStr'),
            ],
          ),
        ));
  }
}
