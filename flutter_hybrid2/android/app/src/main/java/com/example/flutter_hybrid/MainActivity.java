package com.example.flutter_hybrid;

import androidx.annotation.NonNull;

import com.example.flutter_hybrid.basic_message_channel_demo.BasicMessageChannelPlugin;
import com.example.flutter_hybrid.event_channel_demo.EventChannelPlugin;
import com.example.flutter_hybrid.method_channel_demo.LocationPlugin;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity extends FlutterActivity {

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        //注册插件
        flutterEngine.getPlugins().add(new LocationPlugin());
        flutterEngine.getPlugins().add(new BasicMessageChannelPlugin());
        flutterEngine.getPlugins().add(new EventChannelPlugin());

    }



}
