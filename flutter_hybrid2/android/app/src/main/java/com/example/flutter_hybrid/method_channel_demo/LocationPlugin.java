package com.example.flutter_hybrid.method_channel_demo;

import android.app.Activity;
import android.location.Location;

import androidx.annotation.NonNull;

import com.example.flutter_hybrid.GetAddressUtil;
import com.example.flutter_hybrid.LocationUtil;

import java.util.HashMap;
import java.util.Map;

import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * LocationPlugin
 */
public class LocationPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {

    private static final String START_CARD_DETECTION = "getLocation";
    private static final String SET_HINT_INFO = "setHintInfo";

    private Activity mActivity;
    private MethodChannel mMethodChannel;


    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        mMethodChannel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "location_plugin");
        mMethodChannel.setMethodCallHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        //注销通道的监听
//        mMethodChannel.setMethodCallHandler(null);
    }


    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        Log.d("location_plugin:", call.method);
        switch (call.method) {

            case START_CARD_DETECTION:
                getLocation();
                break;
            case SET_HINT_INFO:
              String  infoType = call.argument("info").toString();
                Log.d("====info:", infoType);
              /*  // 跳转原生页面
                Intent jumpToNativeIntent = new Intent(mActivity, NativePageActivity.class);
                jumpToNativeIntent.putExtra("name", (String) call.argument("name"));

                String str = call.argument("name");
                //返回给flutter的参数
                result.success("收到Flutter的消息" + str + " ，返回消息给Flutter!");
                mActivity.startActivity(jumpToNativeIntent);*/
                break;


            default:
                result.notImplemented();
                break;
        }
    }

    private void getLocation() {
        LocationUtil.getCurrentLocation(mActivity, new LocationUtil.LocationCallBack() {
            @Override
            public void onSuccess(Location location) {
                String address = new GetAddressUtil(mActivity).getAddress(location.getLatitude(), location.getLongitude());
                Map<String, Object> resultMap = new HashMap<String, Object>();
                resultMap.put("latitude", location.getLatitude() + "");
                resultMap.put("longitude", location.getLongitude() + "");
                resultMap.put("address", address);
                mMethodChannel.invokeMethod("onGetLocationSuccess", resultMap);
            }

            @Override
            public void onFail(String msg) {
                Log.d("locationpluginfail:", msg);
                Map<String, Object> resultMap = new HashMap<String, Object>();
                resultMap.put("failMsg", msg);
                mMethodChannel.invokeMethod("onGetLocationFail", resultMap);
            }
        });


    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        mActivity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }


}
