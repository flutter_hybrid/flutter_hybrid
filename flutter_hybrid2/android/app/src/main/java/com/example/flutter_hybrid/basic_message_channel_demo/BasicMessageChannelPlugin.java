package com.example.flutter_hybrid.basic_message_channel_demo;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.BasicMessageChannel;
import io.flutter.plugin.common.BasicMessageChannel.MessageHandler;
import io.flutter.plugin.common.StandardMessageCodec;

/**
 * BasicMessageChanneldemo
 */
public class BasicMessageChannelPlugin implements FlutterPlugin, ActivityAware, MessageHandler {
    //Channel的名字
    private static final String BASIC_MESSAGE_CHANNEL = "basic_message_channel_demo";


    private Activity mActivity;
    private BasicMessageChannel mBasicMessageChannel;


    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        mBasicMessageChannel = new BasicMessageChannel<Object>(flutterPluginBinding.getBinaryMessenger(), BASIC_MESSAGE_CHANNEL, StandardMessageCodec.INSTANCE);
        mBasicMessageChannel.setMessageHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        //注销通道的监听
//        mMethodChannel.setMethodCallHandler(null);
    }


    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        mActivity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }


    @Override
    public void onMessage(@Nullable Object message, @NonNull BasicMessageChannel.Reply reply) {
        // 接收flutter数据  并返回数据
        System.out.println("activity-onMessage--flutter传来得数据: " + message);
        reply.reply("这是原生端传过来的信息");

    }
}
