package com.example.flutter_hybrid.event_channel_demo;

import android.os.Handler;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.EventChannel;

/*https://www.jianshu.com/p/5812cf077d6a
1.事件派发对象和派发流的定义
定义事件派发对象和派发流代码如下，在派发流的onListen方法中把系统获取事件派发对象赋值给我们定义的事件派发对象，当系统取消事件派发流时，则把事件派发对象置空即可；
2.派发流的初始化和注册
3.事件派发流回传flutter:在需要的地方执行 eventSink.success(map);
 */
public class EventChannelPlugin implements FlutterPlugin, EventChannel.StreamHandler {
    //Channel的名字
    private static final String EVENT_CHANNEL_NAME = "EventChannelDemo";
    // 事件派发对象
    private EventChannel.EventSink eventSink = null;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        //2.派发流的初始化和注册
        // 初始化事件
        EventChannel eventChannel = new EventChannel(binding.getBinaryMessenger(), EVENT_CHANNEL_NAME);
        eventChannel.setStreamHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {

    }

    @Override
    public void onListen(Object arguments, EventChannel.EventSink events) {
        eventSink = events;
        handler.sendEmptyMessage(0);

    }

    @Override
    public void onCancel(Object arguments) {
        eventSink = null;
    }

    private int voiceValue = 0;
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0:
                    // app的功能逻辑处理
                    //3.事件派发流回传flutter:在需要的地方执行 eventSink.success(map);
                    if (voiceValue >= 100) {
                        // 直接移除，定时器停止
                        voiceValue =0;
                        handler.sendEmptyMessage(1);
                        break;
                    }
                    if (eventSink != null) {
                        HashMap map = new HashMap();
                        map.put("changeVoice", voiceValue += 10);
                        eventSink.success(map);
                    }
                    // 再次发出msg，循环更新
                    handler.sendEmptyMessageDelayed(0, 1000);
                    break;

                case 1:
                    // 直接移除，定时器停止
                    handler.removeMessages(0);
                    break;

                default:
                    break;
            }
        }


    };


}
